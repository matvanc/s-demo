package sk.sygic.demo.adapters;

import java.util.ArrayList;

import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import sk.sygic.demo.R;
import sk.sygic.demo.entities.Person;

public class PersonAdapter extends ArrayAdapter<Person> {
	
	Context context;
    int layoutResId;
    ArrayList<Person> persons = null;
    
    public PersonAdapter(Context context, int layoutResId, ArrayList<Person> persons) {
        super(context, layoutResId, persons);
        
        this.layoutResId = layoutResId;
        this.context = context;
        this.persons = persons;
    }
    
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PersonHolder holder = null;

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(layoutResId, parent, false);

            holder = new PersonHolder();
            
            initHolder(holder, convertView);

            convertView.setTag(holder);
        }
        else
        {
            holder = (PersonHolder) convertView.getTag();
        }

        Person person = persons.get(position);
        
        fillHolder(holder, convertView, person);

        return convertView;
    }
    
    private void initHolder(PersonHolder holder, View convertView) {
        holder.title = (TextView) convertView.findViewById(R.id.title);
        holder.description = (TextView) convertView.findViewById(R.id.description);
    	holder.image = (ImageView) convertView.findViewById(R.id.image);
    }

    static class PersonHolder
    {
        TextView title;
        TextView description;
        ImageView image;
    }
    
    private void fillHolder(PersonHolder holder, View convertView, Person person) {
        holder.title.setText(person.getLastName());
        holder.description.setText(person.getFirstName());
        Picasso.with(context).load(person.getAvatar()).into(holder.image);
    }

}
