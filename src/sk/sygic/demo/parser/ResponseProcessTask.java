package sk.sygic.demo.parser;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;

import android.os.AsyncTask;

public abstract class ResponseProcessTask extends AsyncTask<String, Void, Integer> {
	
	public static final int RESULT_DONE = 0;
	public static final int RESULT_ERROR = 1;
	
	protected static ObjectMapper objectMapper = null;
	protected static JsonFactory jsonFactory = null;
	protected Response response = null;
	
	
	@Override
	protected final void onPreExecute() {
		super.onPreExecute();
		
		if (objectMapper == null) {
			objectMapper = new ObjectMapper();
			jsonFactory = new JsonFactory();
		}
	}
	
	@Override
	protected Integer doInBackground(String... params) {
		return parse(params[0]);
	}
	
	@Override
	protected void onPostExecute(Integer status) {
		super.onPostExecute(status);
		switch(status) {
		case RESULT_ERROR:
			onConnectionError();
			return;
		case RESULT_DONE:
			onDone();
			return;
		}
	}
	
	public Integer parse(String json) {
		try {
			if (json == null) {
				response = null;
			}
			else {
				JsonParser jp = jsonFactory.createParser(json);
				response = objectMapper.readValue(jp, Response.class);
			}
		}
		catch (JsonParseException e) {
			e.printStackTrace();
			return RESULT_ERROR;
		}
		catch (IOException e) {
			e.printStackTrace();
			return RESULT_ERROR;
		}
		
		return RESULT_DONE;
	}

	
	
	protected abstract void onConnectionError();
	
	protected abstract void onDone();
}
