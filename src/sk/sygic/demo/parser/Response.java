package sk.sygic.demo.parser;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

import android.os.Parcel;
import android.os.Parcelable;
import sk.sygic.demo.entities.Department;

public class Response implements Parcelable {
	
	private ArrayList<Department> departments;
	
	

	public ArrayList<Department> getDepartments() {
		return departments;
	}
	
	@JsonProperty("Departments")
	public void setDepartments(ArrayList<Department> departments) {
		this.departments = departments;
	}

	
	
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub

	}

}
