package sk.sygic.demo.database;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.AssetManager;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import sk.sygic.demo.entities.Department;
import sk.sygic.demo.entities.Person;
import sk.sygic.demo.parser.ResponseProcessTask;

public class DatabaseHelper extends SQLiteOpenHelper {
	
	private static final String JSON_FILE = "people.json";
	
	private static final String DATABASE_NAME = "employees";
	private static final int DATABASE_VERSION = 2;
	
	public static final String TABLE_DEPARTMENTS = "departments";
	public static final String DEP_ID = "id";
	public static final String DEP_NAME = "name";
	
	public static final String TABLE_PERSONS = "persons";
	public static final String PER_ID = "id";
	public static final String PER_FIRSTNAME = "first_name";
	public static final String PER_LASTNAME = "last_name";
	public static final String PER_AVATAR = "avatar";
	public static final String PER_DEPARTMENT = "department";
	
	private final Context myContext;
	private DatabaseListener databaseListener;
	
	
	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		
		myContext = context;
	}
	
	public DatabaseHelper(Context context, DatabaseListener l) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		
		myContext = context;
		databaseListener = l;
	}
	
	
	
	static public interface DatabaseListener {
		public void onFilledDatabase();
	}
	
	
   @Override
   public void onCreate(SQLiteDatabase db) {
	   
	   initDatabase(db);
   }
   
   private void initDatabase(SQLiteDatabase db) {
	   db.execSQL("CREATE TABLE departments (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT);");
	   db.execSQL("CREATE TABLE persons (id INTEGER PRIMARY KEY AUTOINCREMENT, first_name TEXT, last_name TEXT, avatar TEXT, department INTEGER, FOREIGN KEY(department) REFERENCES departments(id));");
   }
   
   
   public void startFillingDbFromJson(SQLiteDatabase db) {
	   getAndFillDepartmentsTask(db, JSON_FILE);
   }
   

   private void getAndFillDepartmentsTask(SQLiteDatabase db, String paramJson) {
		
	   StringBuilder buf = new StringBuilder();
		
		BufferedReader in;
		String jsonString = null;
		AssetManager mngr = myContext.getAssets();
		
		try {
			
			
			InputStream json = mngr.open(paramJson);
			in = new BufferedReader(new InputStreamReader(json, "UTF-8"));
			
			while ((jsonString = in.readLine()) != null) {
				  buf.append(jsonString);
			}
			
			in.close();
			
			
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		new ParseAllEmployeesTask(db).execute(buf.toString());
		
		//DemoParser parser = new DemoParser(buf.toString());
		//return parser.getResponse().getDepartments();
   }
   
	private class ParseAllEmployeesTask extends ResponseProcessTask {
		
		private SQLiteDatabase db = null;
		
		public ParseAllEmployeesTask(SQLiteDatabase db) {
			this.db = db;
		}
		

		@Override
		protected void onConnectionError() {
			//some toast message probably
		}

		@Override
		protected void onDone() {
			fillFromDepartments(db, response.getDepartments());
			if (databaseListener != null) {
				databaseListener.onFilledDatabase();
			}
		}
		
	}

   
   private void fillFromDepartments(SQLiteDatabase db, ArrayList<Department> departments) {
	   
	   Department department = new Department();
	   long departmentId;
	   Person person = new Person();
	   
	   for (int i = 0; i < departments.size(); i++) {
		   department = departments.get(i);
		   
		   ContentValues newDep = new ContentValues();
		   newDep.put(DEP_NAME, department.getName());
		   departmentId = db.insert(TABLE_DEPARTMENTS, null, newDep);
		   
		   ArrayList<Person> employees = department.getEmployees();
		   for (int j = 0; j < employees.size(); j++) {
			   person = employees.get(j);
			   
			   ContentValues newEmployee = new ContentValues();
			   newEmployee.put(PER_FIRSTNAME, person.getFirstName());
			   newEmployee.put(PER_LASTNAME, person.getLastName());
			   newEmployee.put(PER_AVATAR, person.getAvatar());
			   newEmployee.put(PER_DEPARTMENT, departmentId);
			   db.insert(TABLE_PERSONS, null, newEmployee);
			   
			   
		   }
		   
	   }
   }

@Override
public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
}
	


}
