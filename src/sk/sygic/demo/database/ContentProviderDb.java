package sk.sygic.demo.database;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

public class ContentProviderDb extends ContentProvider {

	public static final String AUTHORITY = "sk.sygic.demo.contentprovider";
	public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY);
	
	
	private DatabaseHelper dbHelper;

	@Override
	public boolean onCreate() {
		dbHelper = new DatabaseHelper(getContext());
		return true;
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		String table = getTableName(uri);
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		return db.delete(table, selection, selectionArgs);
	}

	@Override
	public String getType(Uri uri) {
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		String table = getTableName(uri);
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		long value = db.insert(table, null, values);
		return Uri.withAppendedPath(CONTENT_URI, String.valueOf(value));
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
		String table = getTableName(uri);
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		Cursor cursor = db.query(table, projection, selection, selectionArgs, null, null, sortOrder);
		return cursor;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
		String table = getTableName(uri);
		SQLiteDatabase db = dbHelper.getWritableDatabase();  
		return db.update(table, values, selection, selectionArgs);
	}
	
	
	

	public static String getTableName(Uri uri) {
		String value = uri.getPath();
		value = value.replace("/", ""); //we need to remove '/'
		return value;
	}

}
