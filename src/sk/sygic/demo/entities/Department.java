package sk.sygic.demo.entities;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

import android.os.Parcel;
import android.os.Parcelable;

public class Department implements Parcelable {
	
	private String name;
	private ArrayList<Person> employees;
	
	
	

	public String getName() {
		return name;
	}

	@JsonProperty("Name")
	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Person> getEmployees() {
		return employees;
	}

	public void setEmployees(ArrayList<Person> employees) {
		this.employees = employees;
	}

	
	
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub

	}

}
