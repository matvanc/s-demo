package sk.sygic.demo;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ListActivity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.RelativeLayout;
import sk.sygic.demo.adapters.PersonAdapter;
import sk.sygic.demo.database.ContentProviderDb;
import sk.sygic.demo.database.DatabaseHelper;
import sk.sygic.demo.entities.Person;
import sk.sygic.demo.parser.ResponseProcessTask;

public class MainActivity extends ListActivity implements DatabaseHelper.DatabaseListener {
	
	public final String DEPARTMENT_NAME = "RD";

	private DatabaseHelper dbHelper;
	private SQLiteDatabase db;
	
	private RelativeLayout rootView;
	//private ListView listView;
	private PersonAdapter adapter;
	private boolean requested;
	
	private ArrayList<Person> persons = new ArrayList<Person>();
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		initDatabase();
		initViews();
	}
	
	private void initDatabase() {
		dbHelper = new DatabaseHelper(this, this);
		db = dbHelper.getReadableDatabase();
		dbHelper.startFillingDbFromJson(db);
	}
	
	private void initViews() {
		rootView = (RelativeLayout) getLayoutInflater().inflate(R.layout.activity_main, null);
	
		adapter = new PersonAdapter(this, R.layout.adapter_item_person, persons);
		getListView().setAdapter(adapter);
	}
	
	@Override
	public void onFilledDatabase() {
		loadPersonsFromDepartment(DEPARTMENT_NAME);
		adapter.notifyDataSetChanged();
	}
	
	
	private void loadPersonsFromDepartment(String department) {
		
		int departId;
		
		Uri contentUri = Uri.withAppendedPath(ContentProviderDb.CONTENT_URI, DatabaseHelper.TABLE_DEPARTMENTS);
		String selectionArgs[] = { department };
		Cursor cursor = getContentResolver().query(contentUri, null, "name=?", selectionArgs, null);
		if (cursor != null) {
			cursor.moveToFirst();	
			departId = cursor.getInt(0);
		}
		else {
			return;
		}
		
		
		
		
		contentUri = Uri.withAppendedPath(ContentProviderDb.CONTENT_URI, DatabaseHelper.TABLE_PERSONS);
		String selectionArgs2[] = { String.valueOf(departId) };
		cursor = getContentResolver().query(contentUri, null, "department=?", selectionArgs2, null);
		cursor.moveToFirst();
		do {
			
			Person person = new Person();
			
			person.setFirstName(cursor.getString(1));
			person.setLastName(cursor.getString(2));
			person.setAvatar(cursor.getString(3));
			
			persons.add(person);
		} while (cursor.moveToNext());
		
		
	}
	
	
	

	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
